#!/bin/sh

SWARM_STATE=$(docker info |grep Swarm: | awk '{print $2}')

if [ "inactive" = "$SWARM_STATE" ]; then
  echo "Initialize Swarm"
  docker swarm init
  exit 1
else
  echo "Swarm already initialized"
  exit 0
fi
